const test = require("ava")
const data = require("./app/index")

const helpers = data()
const aAdvert = { min: { type: "terts",  face: "j", suit: null }, max: { type: "terts",  face: "k", suit: "club" } }
const bAdvert = { min: { type: "terts",  face: "j", suit: null }, max: { type: "terts",  face: "k", suit: "club" } }

test("minFace", async (t) => {
  t.is(helpers.getMinFace("berts", "club"), "10")
})

test("conpareAdverts", (t) => {
  t.is(helpers.compareAdverts(aAdvert, bAdvert, "club"), 0)
  bAdvert.min.face = "9"
  t.is(helpers.compareAdverts(aAdvert, bAdvert, "club"), undefined)
  bAdvert.max.face = "10"
  t.is(helpers.compareAdverts(aAdvert, bAdvert, "club"), 1)
})
const aBonus = { type: "terts", face: "q", suit: null }
test("satisfyBonusAdvert", (t) => {
  t.is(helpers.satisfyBonusAdvert(aBonus, aAdvert, "club"), true)
  aBonus.face = "10"
  t.is(helpers.satisfyBonusAdvert(aBonus, aAdvert, "club"), false)
  aBonus.face = "k"
  t.is(helpers.satisfyBonusAdvert(aBonus, aAdvert, "club"), true)
  aBonus.suit = "spade"
  t.is(helpers.satisfyBonusAdvert(aBonus, aAdvert, "club"), true)
  aBonus.suit = "club"
  t.is(helpers.satisfyBonusAdvert(aBonus, aAdvert, "club"), true)
  aAdvert.max.suit = null
  t.is(helpers.satisfyBonusAdvert(aBonus, aAdvert, "club"), false)
  aBonus.suit = "spade"
  t.is(helpers.satisfyBonusAdvert(aBonus, aAdvert, "club"), true)
})
const bBonus = { type: "terts", face: "q", suit: null }
test("getNextStepBonus", (t) => {
  let result = helpers.getNextStepBonus("more", bBonus, "club")
  t.is(result.face, "q")
  t.is(result.suit, "club")
  bBonus.suit = "spade"
  result = helpers.getNextStepBonus("more", bBonus, "club")
  t.is(result.face, "q")
  t.is(result.suit, "club")
  bBonus.suit = "club"
  result = helpers.getNextStepBonus("more", bBonus, "club")
  t.is(result.face, "k")
  t.is(result.suit, null)
  result = helpers.getNextStepBonus("less", bBonus, "club")
  t.is(result.face, "q")
  t.is(result.suit, null)
  bBonus.suit = "diamond"
  result = helpers.getNextStepBonus("less", bBonus, "club")
  t.is(result.face, "j")
  t.is(result.suit, "club")
})
sAdvert = { min: { type: "terts", face: "j", suit: "club" }, max: { type: "terts", face: "k", suit: null } }
test("getScope", (t) => {
  result = helpers.getScope(true, sAdvert, "club")
  t.is(result.min.face, "j")
  t.is(result.min.suit, "club")
  t.is(result.max.face, "k")
  t.is(result.max.suit, "half")
  result = helpers.getScope(false, sAdvert, "club")
  t.is(result.max.face, "k")
  t.is(result.max.suit, null)
})
const aScope = {
  min: { type: "terts", face: "j", suit: "club" },
  max: { type: "terts", face: "k", suit: null }
}
test("synergyScopes", (t) => {
  result = helpers.synergyScopes(aScope, undefined, "club")
  t.is(result.min.face, "j")
  t.is(result.min.suit, "club")
  t.is(result.max.face, "k")
  t.is(result.max.suit, null)
  const bScope = {
    min: { type: "berts", face: "10", suit: null },
    max: { type: "berts", face: "10", suit: "club" }
  }
  result = helpers.synergyScopes(aScope, bScope, "club")
  t.is(result.min.face, "10")
  t.is(result.min.suit, null)
  t.is(result.max.face, "10")
  t.is(result.max.suit, "club")
  const cScope = {
    min: { type: "terts", face: "q", suit: null },
    max: { type: "terts", face: "q", suit: "club" }
  }
  result = helpers.synergyScopes(aScope, cScope, "club")
  t.is(result.min.face, "q")
  t.is(result.min.suit, null)
  t.is(result.max.face, "k")
  t.is(result.max.suit, null)
})
const sScope = {
  min: { type: "terts", face: "j", suit: "club" },
  max: { type: "terts", face: "k", suit: "club" }
}
test("relationBonusScope", (t) => {
  const bonus = { type: "berts", face: "q" }
  t.is(helpers.relationBonusScope(bonus, sScope, "club").relation, "more")
  bonus.type = "terts"
  bonus.face = "k"
  t.is(helpers.relationBonusScope(bonus, sScope, "club").relation, "inter")
  bonus.suit = "club"
  t.is(helpers.relationBonusScope(bonus, sScope, "club").relation, "more")
  bonus.face = "j"
  t.is(helpers.relationBonusScope(bonus, sScope, "club").relation, "less")
  sScope.min.suit = null
  sScope.max.face = "j"
  sScope.max.suit = null
  bonus.suit = null
  t.is(helpers.relationBonusScope(bonus, sScope, "club").relation, "more")
  sScope.max.suit = "club"
  t.is(helpers.relationBonusScope(bonus, sScope, "club").relation, "inter")
})
