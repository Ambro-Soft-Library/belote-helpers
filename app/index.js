const SIDES = ['south', 'west', 'north', 'east']
const FACES = ['7', '8', '9', '10', 'j', 'q', 'k', 'a']
const SUITS = ['spade', 'heart', 'club', 'diamond']
const BONUS_TYPES = ['terts', 'berts', 'hundred', 'fourCard']
const HEAD_FACES = {
  fourCard: {
    suit: ['10', 'q', 'k', 'a', '9', 'j', '8'],
    nt: ['10', 'j', 'q', 'k', 'a', '8']
  },
  terts: {
    suit: ['9', '10', 'j', 'q', 'k', 'a'],
    nt: ['9', '10', 'j', 'q', 'k', 'a']
  },
  berts: {
    suit: ['10', 'j', 'q', 'k', 'a'],
    nt: ['10', 'j', 'q', 'k', 'a']
  },
  hundred: {
    suit: ['j', 'q', 'k', 'a'],
    nt: ['j', 'q', 'k', 'a']
  }
}
/**
 * type-ისა და trump-ის შესაბამისი HEAD_FACES-ის მიხედვით
 * @param {string} type
 * @param {string} trump
 */
const getMinFace = function (type, trump) {
  try {
    const faces = HEAD_FACES[type][trump === 'nt' ? 'nt' : 'suit']
    return faces[0]
  } finally { }
}
/**
 * type-ისა და trump-ის შესაბამისი HEAD_FACES-ის მიხედვით
 * @param {string} type
 * @param {string} trump
 */
const getMaxFace = function (type, trump) {
  try {
    const faces = HEAD_FACES[type][trump === 'nt' ? 'nt' : 'suit']
    return faces[faces.length - 1]
  } finally { }
}
/**
 * ცალსახად BONUS_TYPES-ის მიხედვით
 * @param {string} a
 * @param {string} b
 */
const compareTypes = function (a, b) {
  try {
    return Math.sign(BONUS_TYPES.indexOf(a) - BONUS_TYPES.indexOf(b))
  } finally { }
}
/**
 * type-ისა და trump-ის შესაბამისი HEAD_FACES-ის მიხედვით
 * @param {string} a
 * @param {string} b
 * @param {string} type
 * @param {string} trump
 */
const compareFaces = function (a, b, type, trump) {
  try {
    const faces = HEAD_FACES[type][trump === 'nt' ? 'nt' : 'suit']
    return Math.sign(faces.indexOf(a) - faces.indexOf(b))
  } finally { }
}
/**
 * მხოლოდ და მხოლოდ trump-ობის მიხედვით
 * @param {string} a
 * @param {string} b
 * @param {string} trump
 */
const compareSuits = function (a, b, trump) {
  try {
    if (a !== trump && b === trump) return -1
    if (a === trump && b !== trump) return 1
    return 0
  } finally { }
}
/**
 * ცარიელი ნაკლებია, დანარჩენი გამომდინარე შესაბამისად type, face, suit-ების ზემოთ აღწერილი შედარებებიდან
 * @param {string} a
 * @param {string} b
 * @param {string} trump
 */
const compareBonuses = function (a, b, trump) {
  try {
    if (!a && !b) return { relation: 'equel' }
    if (!a && b) return { relation: 'less' }
    if (a && !b) return { relation: 'more' }
    let result = compareTypes(a.type, b.type)
    if (result) return { relation: result < 0 ? 'less' : 'more', by: 'type' }
    result = compareFaces(a.face, b.face, a.type, trump)
    if (result) return { relation: result < 0 ? 'less' : 'more', by: 'face' }
    result = compareSuits(a.suit, b.suit, trump)
    if (result) return { relation: result < 0 ? 'less' : 'more', by: 'suit' }
    return { relation: 'equel' }
  } finally { }
}
/**
 * direct-ის მიხედვით აბრუნებს ერთი ბიჯით მეტ ან ნაკლებ ბონუსს
 * თუ მეტი აღარ არსებობს, აბრუნებს {type: 'fourCard', face: null}-ს
 * თუ ნაკლები არ არსებობს, მაშინ {type: null, face: null}
 * @param {string} direct
 * @param {object} bonus
 * @param {string} trump
 */
const getNextStepBonus = function (direct, bonus, trump) {
  try {
    const rtrn = Object.assign({}, bonus)
    switch (direct) {
      case 'more':
        if (bonus.type === 'fourCard') {
          const faces = HEAD_FACES[bonus.type][trump === 'nt' ? 'nt' : 'suit']
          rtrn.face = faces[faces.indexOf(bonus.face) + 1]
        } else if (bonus.suit !== trump) {
          rtrn.suit = trump
        } else {
          rtrn.suit = null
          if (bonus.face !== getMaxFace(bonus.type, trump)) {
            const faces = HEAD_FACES[bonus.type][trump === 'nt' ? 'nt' : 'suit']
            rtrn.face = faces[faces.indexOf(bonus.face) + 1]
          } else {
            rtrn.type = BONUS_TYPES[BONUS_TYPES.indexOf(bonus.type) + 1]
            rtrn.face = getMinFace(rtrn.type, trump)
          }
        }
        break
      case 'less':
        if (bonus.type === 'fourCard') {
          if (bonus.face === getMinFace(bonus.type, trump)) {
            rtrn.type = BONUS_TYPES[BONUS_TYPES.indexOf(bonus.type) - 1]
            rtrn.face = getMaxFace(rtrn.type, trump)
            rtrn.suit = null
          } else {
            const faces = HEAD_FACES[bonus.type][trump === 'nt' ? 'nt' : 'suit']
            rtrn.face = faces[faces.indexOf(bonus.face) - 1]
          }
        } else if (bonus.suit === trump) {
          rtrn.suit = null
        } else {
          rtrn.suit = trump
          if (bonus.face !== getMinFace(bonus.type, trump)) {
            const faces = HEAD_FACES[bonus.type][trump === 'nt' ? 'nt' : 'suit']
            rtrn.face = faces[faces.indexOf(bonus.face) - 1]
          } else {
            rtrn.type = BONUS_TYPES[BONUS_TYPES.indexOf(bonus.type) - 1]
            rtrn.face = rtrn.type ? getMaxFace(rtrn.type, trump) : null
          }
        }
        break
    }
    return rtrn
  } finally { }
}

module.exports = () => {
  return {
    SIDES,
    FACES,
    SUITS,
    BONUS_TYPES,
    HEAD_FACES,
    getMinFace,
    getMaxFace,
    /**
     * type-ის მიხედვით აბრუნებს ბონუსში კარტების რაოდენობას
     * @param {string} type
     */
    getBonusLength: function (type) {
      try {
        return type === 'hundred' ? 5 : type === 'berts' || type === 'fourCard' ? 4 : type === 'terts' ? 3 : 0
      } finally { }
    },
    /**
     * SIDES-ების მიხედვით აბრუნებს side-ის შემდეგ წრეზე მე-order-ე მხარეს
     * თუ side არაა SIDES-ებში, მაშინ SIDES-დან აბრუნებს შემთხვევით ერთ-ერთს
     * @param {string} side
     * @param {number} order
     */
    getNextSide: function (side, order) {
      try {
        if (order === undefined) {
          order = 1
        }
        let index = SIDES.indexOf(side)
        if (index === -1) {
          index = Math.floor(4 * Math.random())
        } else {
          while (index + order < 0) {
            order += 4
          }
          index = (index + order) % 4
        }
        return SIDES[index]
      } finally { }
    },
    compareTypes,
    compareFaces,
    compareSuits,
    compareBonuses,
    /**
     * ტოლია თუ ზედები და ქვედები, როგორც ბონუსები, ტოლია;
     * მეტია თუ ერთი მეორეზე მკაცრად მეტია ან შეხების წერტილი არის trump-იანი;
     * თუ ორივე "ვიწროა":
     * მხოლოდ ერთს (ისაა სწორედ მეტი) min, max suit-ები ორივე trump-ები აქვს
     * (რადგან ამის შედარებამდე ჩავიდა ე.ი. მეორეს აქვს min.suit = null, max.suit=trump);
     * @param {object} a
     * @param {object} b
     * @param {string} trump
     */
    compareAdverts: function (a, b, trump) {
      try {
        if (!a && !b) return 0
        if (!a && b) return -1
        if (a && !b) return 1
        const compType = compareTypes(a.type, b.type)
        if (compType) return compType
        const compMaxMax = compareBonuses(a.max, b.max, trump).relation
        const compMinMin = compareBonuses(a.min, b.min, trump).relation
        if (compMaxMax === 'equel' && compMinMin === 'equel') return 0
        const compMaxMin = compareBonuses(a.max, b.min, trump).relation
        if (compMaxMin === 'less' || (compMaxMin === 'equel' && a.max.suit === trump)) return -1
        const compMinMax = compareBonuses(a.min, b.max, trump).relation
        if (compMinMax === 'more' || (compMinMax === 'equel' && a.min.suit === trump)) return 1
        return undefined
      } finally { }
    },
    /**
     * არის თუ არა ბონუსი განაცხადის ფარგლებში
     * @param {object} bonus
     * @param {object} advert
     * @param {string} trump
     */
    satisfyBonusAdvert: function (bonus, advert, trump) {
      try {
        if (!bonus) return false
        if (!advert) return true
        if (!advert.min) advert.min = { type: advert.type, face: getMinFace(bonus.type, trump) }
        if (!advert.max) advert.max = { type: advert.type, face: getMaxFace(bonus.type, trump) }
        if (!advert.min) advert.min = { type: advert.type, face: getMinFace(bonus.type, trump) }
        if (!advert.max) advert.max = { type: advert.type, face: getMaxFace(bonus.type, trump) }
        const bottom = compareBonuses(
          bonus,
          advert.min,
          trump
        )
        const top = compareBonuses(
          bonus,
          advert.max,
          trump
        )
        return ['more', 'equel'].includes(bottom.relation) && ['less', 'equel'].includes(top.relation)
      } finally { }
    },
    getNextStepBonus,
    /**
     * განაცხადიდან ქმნის შეზღუდვას, შეიძლება half-ით "აწევით"
     * იმ შემთხვევისთვის, როცა მოწინააღმდეგს ასწრებს ბონუსის ჩვენებას
     * @param {boolean} increase
     * @param {object} advert
     * @param {string} trump
     */
    getScope: function (increase, advert, trump) {
      try {
        if (!advert) {
          return null
        }
        const rtrn = {
          min: { type: advert.min.type, face: advert.min.face, suit: advert.min.suit },
          max: { type: advert.max.type, face: advert.max.face, suit: advert.max.suit }
        }
        if (increase) {
          if (advert.min.type !== 'fourCard') {
            rtrn.min.suit = rtrn.min.suit === trump ? trump : 'half'
          }
          if (advert.max.type !== 'fourCard') {
            rtrn.max.suit = rtrn.max.suit === trump ? trump : 'half'
          }
        }
        return rtrn
      } finally { }
    },
    /**
     * აერთიანებს ორ შეზღუდვას ერთში
     * @param {object} a
     * @param {object} b
     * @param {string} trump
     */
    synergyScopes: function (a, b, trump) {
      try {
        if (!a && !b) {
          return null
        }
        let rtrn = {}
        if (!a) {
          rtrn = b
        } else if (!b) {
          rtrn = a
        } else {
          if (compareBonuses(a.min, b.min, trump).relation === 'more') {
            rtrn.min = a.min
          } else if (compareBonuses(a.min, b.min, trump).relation === 'equel') {
            if (a.min.suit === trump || a.min.suit === 'half') {
              rtrn.min = a.min
            } else {
              rtrn.min = b.min
            }
          } else {
            rtrn.min = b.min
          }
          if (compareBonuses(a.max, b.max, trump).relation === 'more') {
            rtrn.max = a.max
          } else if (compareBonuses(a.max, b.max, trump).relation === 'equel') {
            if (a.max.suit === trump || a.max.suit === 'half') {
              rtrn.max = a.max
            } else {
              rtrn.max = b.max
            }
          } else {
            rtrn.max = b.max
          }
        }
        return rtrn
      } finally { }
    },
    /**
     * ბონუსის მიმართებას შეზღუდვისადმი
     * თუ ბონუსი უდრის ზედა ზღვარს და suit trump-ია, მაშინ ითვლება ბონუსი მეტია
     * თუ ბონუსი უდრის ქვედა ზღვარს და suit trump-ია, მაშინ ითვლება ბონუსი ნაკლებია
     * @param {object} bonus
     * @param {object} scope
     * @param {string} trump
     */
    relationBonusScope: function (bonus, scope, trump) {
      try {
        if (!scope) return { relation: 'more' }
        const min = Object.assign({}, scope.min)
        const max = Object.assign({}, scope.max)
        if (bonus.suit === trump) {
          min.suit = min.suit === 'half' ? null : min.suit
          max.suit = max.suit === 'half' ? null : max.suit
        } else {
          min.suit = min.suit === 'half' ? trump : min.suit
          max.suit = max.suit === 'half' ? trump : max.suit
        }
        const top = compareBonuses(bonus, max, trump)
        if (top.relation === 'more' || top.relation === 'equel') return { relation: 'more' }
        const bottom = compareBonuses(bonus, min, trump)
        if (bottom.relation === 'less' || (bottom.relation === 'equel' && bonus.suit === trump))
          return { relation: 'less' }
        return { relation: 'inter' }
      } finally { }
    }
  }
}
