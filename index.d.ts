/**
 * The method must call when library is used on client side.
 */
export default function (): object

/**
 * The method must call when library is used on server side.
 */
export function beloteHelpersServer(): object
